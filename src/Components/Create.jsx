import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import fire from '../fire';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import axios from 'axios';
import Menu from './Menu';



class Create extends Component {
    constructor() {
        super();
        this.ref = fire.firestore().collection('regionList');
        this.state = {
            regionName: '',
            available: 'disponible',
            radioNotAvailable:'----------',
            tipo: '',
            activo: false
          
        };
      }
      onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
      }
    
      onSubmit = (e) => {
        e.preventDefault();
        console.log("select",e)
    this.setState({activo:true})
        const {  regionName, available, radioNotAvailable, tipo } = this.state;
        axios.post('https://us-central1-prueba1-ce25a.cloudfunctions.net/registerRepresentativePoint',{regionName, available, radioNotAvailable, tipo})
    
        .then((docRef) => {
          this.setState({activo:false})
          this.props.history.push("/")
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
      }

    render() {
        const {  regionName, available, radioNotAvailable, tipo } = this.state;
        return (
          <div>
          <Menu/>
            <div className="login-wrap">

              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">
                  
                  </h3>

                </div>
                
                <div style={{paddingTop:"6%"}} className="login-html"  >
                <h3 className="panel-title">
                    AGREGAR LUGAR
                    <br/>
                  </h3>
                  
                  <form  onSubmit={this.onSubmit}>
                    <div   className="form-group">
                      <label for="title">Nombre:</label>
                      <input type="text" className="form-control" name="regionName" value={regionName} onChange={this.onChange} placeholder="Name" required />
                    </div>
                    <div className="form-group">
                      <label for="description" >Estado:</label>
                      <Form.Control as="select"  defaultValue="disponible" name="available" onChange={this.onChange}  cols="80" rows="3">{available}>
                       <option>Disponible</option>
                        <option>No Disponible</option>
                       </Form.Control>
                    </div>
                    
                    {console.log("estado", this.state.available)}
                    {this.state.available==="No disponible"  &&
                    <>
                     <div   className="form-group">
                      <label for="title">Radio:</label>
                      <input type="Number" className="form-control" name="radioNotAvailable" value={radioNotAvailable} onChange={this.onChange} placeholder="Radio" required />
                    </div>
                
                    <div   className="form-group">
                      <label for="title">Tipo:</label>
                      <input type="text" className="form-control" name="tipo" value={tipo} onChange={this.onChange} placeholder="Tipo de Lugar" required />
                    </div>
                       </>} 
                              
                    <button disabled={this.state.activo} type="submit" className="btn btn-primary btn-block">Enviar</button><br/>
                    <h4><Link to="/" className="btn btn-primary btn-block" >Lista de Lugares</Link></h4>
                  </form>
                </div>
              </div>
            </div>
            </div>
          );
        }
      }

export default Create;