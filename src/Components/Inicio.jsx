import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Loader from "react-loader-spinner";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";


import fire from "../fire";
import { Container } from "react-bootstrap";


class Inicio extends Component {
  constructor(props) {
    super(props);
    this.ref = fire.firestore().collection("regionList");
    this.unsubscribe = null;
    this.state = {
      regionList: [],
      Loading: false,
      search: "",
      cantidad: 5,
      tipo: ""
    };
  }

  onChange = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  filtrar = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);

    console.log("tipo", this.state.tipo);
    const { tipo } = this.state;
    axios
      .post("https://us-central1-prueba1-ce25a.cloudfunctions.net/filtroSelect", {
        tipo
      })

      .then(response => {
        const ListaRegion = response.data.regionsList;
        this.setState({ activo: false, regionList: ListaRegion });
        console.log("iqweiq", ListaRegion);
      })
      .catch(error => {
        console.error("Error adding document: ", error);
      });
  };

  CargarDatos = () => {
    this.setState({
      Loading: true
    });
    axios
      .get("https://us-central1-prueba1-ce25a.cloudfunctions.net/getRegionsList")
      .then(response => {
        const ListaRegion = response.data.regionsList;

        console.log("listaregion", ListaRegion);
        this.setState({
          regionList: ListaRegion,
          Loading: false
        });
      });
  };

  /* onCollectionUpdate = querySnapshot => {
    const regionList = [];
    querySnapshot.forEach(doc => {
      console.log("doc", doc);
      const { regionName, available, radioNotAvailable, tipo } = doc.data();
      regionList.push({
        key: doc.id,
        doc, // DocumentSnapshot
        regionName,
        available,
        radioNotAvailable,
        tipo
      });
    });
    this.setState({
      regionList
    });
  }; */

  componentDidMount() {
    // this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    this.CargarDatos();
  }

  

  cargarMas = () => {
    this.setState({ cantidad: this.state.cantidad + 5 });
    const cantidad = this.state.cantidad;
    console.log("cantidad", this.state.cantidad);
    axios
      .post(
        "https://us-central1-prueba1-ce25a.cloudfunctions.net/cargarLista",
        { cantidad }
      )

      .then(response => {
        const ListaRegion = response.data.regionsList;
        this.setState({ activo: false, regionList: ListaRegion });
        console.log("iqweiq", ListaRegion);
      })
      .catch(error => {
        console.error("Error adding document: ", error);
      });
  };

  render() {
    console.log("estado", this.state);
    const { search, Loading } = this.state;
  
    let data;

    if (Loading) {
      data = (
        <div style={{ paddingTop: "6%" }}>
          <Loader
            type="BallTriangle"
            color="#00BFFF"
            height={200}
            width={200}
          />{" "}
        </div>
      );
    } else {
      data = (
        <div>
          <br />
          <div className="container">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title">LISTA DE LUGARES</h4><br/>
                <div>
                  <Row>
                    <Col>
                 
                      <input
                        type="text"
                        className="form-control"
                        name="search"
                        value={search}
                        onChange={this.onChange}
                        placeholder="Buscar"
                      />
                    </Col>
                    <Col>
                    <div class="form-group">
                      
                     <select
                          onChange={this.filtrar}
                          name="tipo"
                          class="form-control"
                          id="exampleFormControlSelect1"
                        >
                          <option value="" disabled selected>
                            seleccionar
                          </option>
                          <option>Colonia</option>
                          <option>Hospital</option>
                          <option>Empresa</option>
                          <option>Colegio</option>
                        </select>
                        </div> 
                        </Col>
                       <Col> <button
                          onClick={this.CargarDatos}
                          type="submit"
                          className="btn btn-block btn-primary "
                        >
                          Limpiar
                        </button></Col>
                     
                        <Col>  <Link to="/create" className="btn btn-block btn-primary ">
                  Agregar{" "}
                </Link></Col>
             
                     
                    
                  </Row>
                </div>
              </div>
              <div className="panel-body">
                <table className="table table-bordered">
                  <thead className="thead-dark">
                    <tr>
                      <th>Name</th>
                      <th>Estado</th>
                      <th>Radio</th>
                      <th>Tipo de Lugar</th>
                      <th>Descripcion</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.regionList
                      .filter(region =>
                        region.regionName.includes(this.state.search)
                      )
                      .map(regionList => (
                        <tr>
                          <td>{regionList.regionName}</td>
                          {regionList.available==true?
                          <td>Disponible</td>: <td>No disponible</td>}
                          <td>{regionList.radioNotAvailable}</td>
                          <td>{regionList.tipo}</td>
                          <td>
                            {" "}
                            <Link to={`/show/${regionList.id}`}>
                              <button className="btn btn-sm btn-outline-primary">
                                Ver
                              </button>
                            </Link>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            
        
               
              <button
                onClick={this.cargarMas}
                className="btn btn-lg btn-primary "
              >
                Cargar Mas{" "}
              </button>
            </div>
          </div>
        </div>
      );
    }
    console.log(this.state.Loading);
    return <div>{data}</div>;
  }
}

export default Inicio;
