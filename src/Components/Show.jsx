import React, { Component } from "react";
import fire from "../fire";
import { Link } from "react-router-dom";
import Menu from "./Menu";
import axios from "axios";

import Loader from "react-loader-spinner";
import "./styles/show.css";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import ModalRepresentativo from "./ModalRepresentativo";

class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LugarEspecifico: {},
      key: "",
      Latitud: "",
      Longitud:"",
      regionCode: "",
      Loading: false,
      puntosRepresentativos: []
    };
  }

  componentDidMount() {
   

    this.CargarRegionList();
    this.CargarDatos();
  }

  MostrarModal = () => {
    this.setState({
      visible: true
    });
  };
  CerrarModal = () => {
    this.setState({
      visible: false
    });
  };

  CargarRegionList = () => {
    const regionCode = this.props.match.params.id;
    console.log("respuesta", regionCode);

    if (regionCode) {
      const obj = {
        regionCode: regionCode
      };
      axios
        .post(
          "https://us-central1-prueba1-ce25a.cloudfunctions.net/ObtenerLugarEspecifico",
          obj
        )

        .then(response => {
          console.log("respuesta", response.data);
          const LugarEspecifico = response.data;
          console.log("LugarEspecifico", LugarEspecifico);

          this.setState({
            LugarEspecifico: LugarEspecifico
          });
        })
        .catch(error => {
          console.error("Error adding document: ", error);
        });
    }
  };

  CargarDatos = () => {
    const regionCode = this.props.match.params.id;
    console.log("respuesdfsdsta", regionCode);

    if (regionCode) {
      const obj = {
        regionCode: regionCode
      };
      axios
        .post(
          "https://us-central1-prueba1-ce25a.cloudfunctions.net/ObtenerPuntosRepresentativos",
          obj
        )

        .then(response => {
          console.log("k,,ko,3ko,4", response);
          const puntosRepresentativos =
            response.data.ObtenerPuntosRepresentativos;

          this.setState({
            puntosRepresentativos: puntosRepresentativos
          });
        })
        .catch(error => {
          console.error("Error adding document: ", error);
        });
    }
  };

  removeRegion = regionCode => {
    Swal.fire({
      title: "Eliminar?",
      text: "Eliminar",
      type: "Warning",
      showCancelButton: true,
      confirmButtonText: "Estas seguro de eliminar",
      cancelBurttonText: "No, Cancelar"
    }).then(result => {
      if (result.value) {
        console.log("lllllll", regionCode);

        if (regionCode) {
          const obj = {
            regionCode: regionCode
          };
          this.setState({
            Loading: true
          });
          axios
            .post(
              "https://us-central1-prueba1-ce25a.cloudfunctions.net/removeRegion",
              obj
            )

            .then(response => {
              this.setState({
                Loading: false
              });
              this.props.history.push("/");
              console.log("respuesta", response);
            })
            .catch(error => {
              console.error("Error adding document: ", error);
            });
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("Cancelado");
      }
    });
  };


  removePunto = (regionCode, pointCode) => {
    console.log("punto",pointCode)
    Swal.fire({
      title: "Eliminar?",
      text: "Eliminar",
      type: "Warning",
     
    }).then(result => {
      if (result.value) {
        console.log("lllllll", regionCode);

        if (regionCode) {
          const obj = {
            regionCode: regionCode,
            pointCode:pointCode
            
          };
          this.setState({
            Loading: true
          });
          axios
            .post(
              "https://us-central1-prueba1-ce25a.cloudfunctions.net/eliminarPuntoRepresentativo ",
              obj
            )

            .then(response => {
              this.setState({
                Loading: false
              });
              this.CargarDatos()
            })
            .catch(error => {
              console.error("Error adding document: ", error);
            });
        }
      }
    });
  };


  render() {
    const {
      available,
      regionName,
      radioNotAvailable,
      tipo
    } = this.state.LugarEspecifico;
    const { puntosRepresentativos } = this.state;
    console.log("3333", this.state.puntosRepresentativos);
    console.log("producto", this.props);

    const { Loading } = this.state;
    let data;
    if (Loading) {
      data = (
        <div style={{ paddingTop: "5%", marginLeft: "42%" }}>
          <Loader
            type="BallTriangle"
            color="#00BFFF"
            height={200}
            width={200}
          />{" "}
        </div>
      );
    } else {
      data = (
        <>
          <div className="login-wrap">
            <div style={{ paddingTop: "6%" }} className="login-html">
              <div class="container">
                <div class="panel panel-default">
                  <h1>INFORMACION</h1>
                  <div className="letra">
                    <h1 className="disponible">Nombre:</h1>

                    <h2 className="tituloDisponible">
                      {" "}
                      {this.state.LugarEspecifico.regionName}
                    </h2>
                  </div>
                </div>

                <div className="letra">
                  <h1 className="disponible"> Disponible: </h1>{" "}
                 
                    {" "}
                    {( this.state.LugarEspecifico.available==true)?<h2 className="tituloDisponible"> Disponible</h2>:<h2 className="tituloDisponible">No disponible</h2>}
                   
                
                </div>
                {console.log("estado", this.state.available)}
                {available ==false && (
                  <>
                    <div className="letra">
                      <h1 className="disponible">Radio:</h1>
                      <h2 className="tituloDisponible">
                        {" "}
                        {this.state.LugarEspecifico.radioNotAvailable}
                      </h2>
                    </div>
                    <div className="letra">
                      <h1 className="disponible">Tipo:</h1>
                      <h2 className="tituloDisponible">
                        {" "}
                        {this.state.LugarEspecifico.tipo}
                      </h2>
                    </div>{" "}
                  </>
                )}

                <br />
                <br />
                <br />
                <Link
                  to={`/edit/${this.props.match.params.id}`}
                  class="btn btn-success btn-block"
                >
                  Editar
                </Link>
                <button
                  onClick={() => this.removeRegion(this.props.match.params.id)}
                  class="btn btn-danger btn-block"
                >
                 Eliminar
                </button>
                <Link to="/" className="btn btn-primary btn-block">
                  Lista de Lugares {" "}
                </Link>
                <br /><br/><br/><br/><br/>
                <button 
              onClick={() => this.MostrarModal()}
              class="btn btn-primary btn-block "
            >
              Agregar Punto de Referencia
            </button>
              </div>
            </div>
          </div>

          <div>
            <ModalRepresentativo
              visible={this.state.visible}
              Cerrar={this.CerrarModal}
              obtenerReferencia={this.CargarDatos}
              regionCode={this.props.match.params.id}
            />
            <br />
           
          </div>
         
          {puntosRepresentativos.length>0 &&(

         
          <div >
            <h2 className="tituloDisponible"> </h2>
            <div className="tablaRepresentativa">
            <table className="table table-bordered">
              <thead className="thead-dark">
                <tr className="columna">
                  <th>Coordenada</th>
                  <th>Accion</th>
                </tr>
              </thead>
              <tbody>
                {puntosRepresentativos.map(punto => (
                  
                  <tr>
                    
                <td>{punto.Latitud },{punto.Longitud}</td>
                    <td >  <button  
                  onClick={() => this.removePunto(punto.regionCode,punto.pointCode)}
                  class="btn btn-block btn-outline-danger"
                >
                  Eliminar
                </button></td>
                   
                  </tr>
              
                ))}
              </tbody>
            </table>
            
            </div>
            
            

           

          </div>
          )}
        </>
      );
    }

    return (
      <div>
        <Menu />
        <div>
          {" "}
          <br />
          {data}
        </div>
      </div>
    );
  }
}

export default Show;
