import ReactDOM from "react-dom";
import Modal from "react-bootstrap/Modal";
import axios from 'axios';
import fire from '../fire';
import ModalBody from "react-bootstrap/ModalBody";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalFooter from "react-bootstrap/ModalFooter";
import ModalTitle from "react-bootstrap/ModalTitle";
import React, { Component } from 'react';

class ModalRepresentativo extends Component {
  constructor(props) {
    super(props);
    this.ref = fire.firestore().collection('regionList');
    this.state = {
      
            Latitud: '',
            Longitud: '',
            activo: false
    };
  }

  Cerrar1 =()=>{
  this.props.Cerrar()
    
  }
  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit = () => {
    this.setState({activo:true})
    const { regionCode}=this.props;
    const {  Latitud, Longitud } = this.state;
    axios.post('https://us-central1-prueba1-ce25a.cloudfunctions.net/registrarPuntosdeReferencia',{regionCode, Latitud, Longitud})

    .then((docRef) => {
      this.setState({activo:false})
      console.log("obtener",this.props)
     this.props.obtenerReferencia() 
     this.props.Cerrar()
     
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }
   
  render() {
   
    const {Cerrar,visible, regionCode}=this.props;
    console.log("regionCode",regionCode)
    console.log("hola", this.state.regionName)


    return (
      <div onSubmit={this.onSubmit}>
        <Modal show={visible}>
      <ModalHeader>
        <ModalTitle>Agregar Punto de Referencia</ModalTitle>
      </ModalHeader>
      <ModalBody> <input type="Number" className="form-control" name="Latitud"  onChange={this.onChange} placeholder="Latitud" required /></ModalBody>
      <ModalBody> <input type="Number" className="form-control" name="Longitud"  onChange={this.onChange} placeholder="Longitud" required /></ModalBody>
      <ModalFooter><button disabled={this.state.activo}  type="submit" onClick={()=>this.onSubmit()}  class="btn btn-primary btn-block">Enviar</button></ModalFooter>
      <ModalFooter><button onClick={()=>this.Cerrar1()}  class="btn btn-danger btn-block">Cerrar</button></ModalFooter>
    </Modal>
      </div>
    );
  }
}

export default ModalRepresentativo;
