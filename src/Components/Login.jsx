import React, { Component } from "react";
import fire from "../fire";
import "./Login.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  login = e => {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {
        console.log(u);
      })
      .catch(err => {
        console.log(err);
      });
  };

  signup = e => {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {
        console.log(u);
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      ///* Formulario de iniciar sesion
      <div>
        <br /> <br />
        <div class="login-wrap">
          <div class="login-html">
            <h2>
              <br />
              Login
            </h2>
            <input id="tab-1" type="radio" name="tab" class="sign-in" checked />
            <label for="tab-1" class="tab"></label>
            <input id="tab-2" type="radio" name="tab" class="sign-up" />
            <label class="tab"></label>
            <div class="login-form">
              <div class="sign-in-htm">
                <div class="group">
                  <label for="user" class="label">
                    Usuario
                  </label>
                  <input
                    id="user"
                    class="input"
                    type="email"
                    id="email"
                    name="email"
                    placeholder="Correo"
                    onChange={this.handleChange}
                    value={this.state.email}
                    required
                  />
                </div>
                <div class="group">
                  <label for="pass" class="label">
                    Password
                  </label>
                  <input
                    id="pass"
                    type="password"
                    class="input"
                    data-type="password"
                    name="password"
                    type="password"
                    onChange={this.handleChange}
                    id="password"
                    placeholder="Introducir Contraseña"
                    value={this.state.password}
                  />
                </div>
                <div class="group">
                  <input id="check" type="checkbox" class="check" checked />
                </div>
                <div class="group">
                  <input
                    onClick={this.login}
                    type="submit"
                    class="button"
                    value="Iniciar Sesion"
                  />
                </div>
                <div class="hr"></div>
                <div class="foot-lnk"></div>
              </div>

              <div class="foot-lnk"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
