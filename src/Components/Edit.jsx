import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import fire from '../fire';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import Menu from './Menu';
import axios from 'axios';

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
          key: '',
          regionName: '',
          available: '',
          radioNotAvailable: '',
          tipo: '',
          activo: false
          
         
        };
      }

      componentDidMount() {
        const regionCode= this.props.match.params.id
        this.CargarDatos(regionCode)

      
      }

      CargarDatos =(regionCode)=>{

        console.log("respuesta",regionCode)
        
        if (regionCode) {
          const obj={
            regionCode:regionCode
          }
          axios.post('https://us-central1-prueba1-ce25a.cloudfunctions.net/getRepresentativePoints',obj)
   
          .then((response) => {
            console.log("respuesta",response)
            const id = response.data.regionList[0].id;
            const regionName = response.data.regionList[0].regionName;
            const available = response.data.regionList[0].available;
            const radioNotAvailable = response.data.regionList[0].radioNotAvailable;
            const tipo = response.data.regionList[0].tipo;
           
        
            this.setState({
             
              key: id,
              regionName: regionName,
              available:available,
              radioNotAvailable: radioNotAvailable,
              tipo: tipo
           });
            
          })
          .catch((error) => {
            console.error("Error adding document: ", error);
          });
        }
        }

        
       
    
      onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState({regionList:state});
      }
    
      onSubmit = (e) => {
        e.preventDefault();
        this.setState({activo:true})
     

        const { regionName, available, radioNotAvailable, tipo } = this.state;
        let radio= "";
        let tipo2="";
        if (available==="Disponible") {
       radio="-------";
       tipo2="";
        
        }else{
          radio=radioNotAvailable;
          tipo2=tipo;
        }
        const regionCode= this.props.match.params.id
         const obj={
            regionCode:regionCode,
            regionName:regionName,
            available:available,
            radioNotAvailable:radio,
            tipo: tipo2
          }
    

 axios.post( 'https://us-central1-prueba1-ce25a.cloudfunctions.net/updateRegion',obj)
    
      

        .then((docRef) => {
         
          this.setState({
            key: '',
           regionName: '',
          available: '',
          radioNotAvailable: '',
          tipo: ''
         
          });
          this.setState({activo:false})
          this.props.history.push("/show/"+this.props.match.params.id)
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
      }

    
    render() {
      const {  regionName, available, radioNotAvailable, tipo } = this.state;
        return (

          <div>
          <Menu/>
         <br/> 
          <div className="login-wrap">
          <div style={{paddingTop:"6%"}} className="login-html"  >
            <div className="container">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">
                    EDITAR LUGAR
                    <br/>
                  </h3>
                </div>
                <div className="panel-body">
                 
                  <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                      <label for="title">Nombre:</label>
                      <input type="text" className="form-control" name="regionName" value={this.state.regionName} onChange={this.onChange} placeholder="regionName" />
                    </div>
                   <div className="form-group">
                      <label for="description" >Estado:</label>
                      <Form.Control as="select"  value={this.state.available} name="available" onChange={this.onChange}  cols="80" rows="3">{available}>
                       <option>Disponible</option>
                        <option>No Disponible</option>
                       </Form.Control>
                    </div>
                    {console.log("estado", this.state.available)}
                    {this.state.available==="No disponible"  &&
                    <>
                    <div   className="form-group">
                      <label for="title">Radio:</label>
                      <input type="Number" className="form-control" name="radioNotAvailable" value={radioNotAvailable} onChange={this.onChange} placeholder="Radio" required />
                    </div>

                    <div className="form-group">
                      <label for="title">Tipo:</label>
                      <input type="text" className="form-control" name="tipo" value={this.state.tipo} onChange={this.onChange} placeholder="tipo" />
                   <br/> </div>
                   </>} 
      
                    <button  disabled={this.state.activo} type="submit" className="btn btn-success btn-block">Enviar</button> 
                  <Link to={`/show/${this.state.key}`} className="btn btn-primary btn-block">Informacion Editada</Link>                    
                  </form>
                </div>
              </div>
            </div>
            </div>
            </div>
            </div>
          );
        }
      }

export default Edit;





