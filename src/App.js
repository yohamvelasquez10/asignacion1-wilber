import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import fire from "./fire";
import Home from './Components/Home';
import Login from './Components/Login';




class App extends Component{
  constructor(props)
  {
    super(props);
    this.state={
      user : {}
    }
  }

componentDidMount()
{
  this.authListener();
}

authListener(){
    fire.auth().onAuthStateChanged((user)=>{
      if(user)
      {
        this.setState({user})
      }
      else{
        this.setState({user : null})
      }
    }
    )}

  render(){
    return (
      <div className="App">
        {this.state.user ? (<Home/>) : (<Login/>)}
       
      </div>
      
     
    );
  }
}





export default App;
