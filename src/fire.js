import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyBPx-IUQ5f3g9xhKaR1n_itiJ07GDVAkHM",
    authDomain: "asignacion1-88dc9.firebaseapp.com",
    databaseURL: "https://asignacion1-88dc9.firebaseio.com",
    projectId: "asignacion1-88dc9",
    storageBucket: "asignacion1-88dc9.appspot.com",
    messagingSenderId: "451027875419",
    appId: "1:451027875419:web:f45f22eabac3ce116b717e"
  };

  const fire = firebase.initializeApp(firebaseConfig);

  export default fire;